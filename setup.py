from setuptools import find_packages, setup

package_name = 'obstacle_avoidance_lidar'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='GueLaKais',
    maintainer_email='koroyeldiores@gmail.com',
    description='TODO: Package description',
    license='Apache-2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'lidar_obstacle_avoidance = obstacle_avoidance_lidar.lidar_obstacle_avoidance:main'
        ],
    },
)
