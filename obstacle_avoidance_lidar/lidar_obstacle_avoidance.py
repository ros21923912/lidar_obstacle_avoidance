#!/usr/bin/env python3
import rclpy
from geometry_msgs.msg import Vector3Stamped #ros msg that deals with moving the robot
from sensor_msgs.msg import LaserScan #ros msg that gets the laser scans
class Avoider:
	''' This class provides simple obstacle avoidance functionalities to a ROS robot '''

	# This dict keeps track of the distance measures for each region
	Regions_Report = {
	                     "front_C": [], "front_L": [], "left_R" : [],
	                     "left_C" : [], "left_L" : [], "back_R" : [],
	                     "back_C" : [], "back_L" : [], "right_R": [],
	                     "right_C": [], "right_L": [], "front_R": [],
	                 }
	# These are the costs to deviate from each region to the goal region (front_C)
	Regions_Distances = {
	                     "front_C":  0, "front_L":  1, "left_R" :  2,
	                     "left_C" :  3, "left_L" :  4, "back_R" :  5,
	                     "back_C" :  6, "back_L" : -5, "right_R": -4,
	                     "right_C": -3, "right_L": -2, "front_R": -1,
	                 	}

	def __init__(self, vel_obj,node, obstacle_threshold=0.5, regional_angle=30, normal_lin_vel=0.5,trans_lin_vel=-0.09,trans_ang_vel=1.75):
		self.vel_obj = vel_obj
		self.node = node
		self.OBSTACLE_DIST = obstacle_threshold
		self.REGIONAL_ANGLE = regional_angle
		self.NORMAL_LIN_VEL = normal_lin_vel
		self.TRANS_LIN_VEL = trans_lin_vel
		self.TRANS_ANG_VEL = trans_ang_vel

	def indentify_regions(self, scan) -> None:
		# This list keeps track of the order in which the regions' readings are obtained
		REGIONS = [
		    "front_C", "front_L", "left_R" ,
		    "left_C" , "left_L" , "back_R" ,
		    "back_C" , "back_L" , "right_R",
		    "right_C", "right_L", "front_R",
		]

		# The front central region necessitate getting the last and first 15 points of the ranges
		intermediary = scan.ranges[
		    :int(self.REGIONAL_ANGLE/2)] + scan.ranges[(len(scan.ranges)-1)*int(self.REGIONAL_ANGLE/2):]
		self.Regions_Report["front_C"] = [x for x in intermediary if x <= self.OBSTACLE_DIST and x != 'inf']
		
		# Enumerate all regions but the first
		for i, region in enumerate(REGIONS[1:]):
			# Only objects at a distance less than or equal to the threshold are considered obstacles
			self.Regions_Report[region] = [
			    x for x in scan.ranges[self.REGIONAL_ANGLE*i:self.REGIONAL_ANGLE*(i+1)] if x <= self.OBSTACLE_DIST and x != 'inf']

	def clearance_test(self):
		goal = "front_C"
		closest = 10e6
		regional_dist = 0
		maxima = {"destination": "back_C", "distance": 10e-6}
		for region in self.Regions_Report.items():
			regional_dist = abs(self.Regions_Distances[region[0]]-self.Regions_Distances[goal])
			#if there're no obstacles in that region
			if not len(region[1]) and (regional_dist < closest):
				closest = regional_dist
				maxima["distance"] = self.OBSTACLE_DIST
				maxima["destination"] = region[0]
			#check if it's the clearest option
			elif(max(region[1] if region[1] else [0]) > maxima["distance"]):
				maxima["distance"] = max(region[1])
				maxima["destination"] = region[0]
		#calculate the cost to the chosen orientation
		regional_dist = self.Regions_Distances[maxima["destination"]]-self.Regions_Distances[goal]
		# Return whether to act or not, and the angular velocity with the appropriate sign
		return (closest != 0), (regional_dist/[abs(regional_dist) if regional_dist != 0 else 1][0])*self.TRANS_ANG_VEL

	def avoid(self) -> Vector3Stamped:
		act, ang_vel = self.clearance_test()
		# If act is False, and_vel is set to 0, so the robot won't move
		self.vel_obj.header.stamp = self.node.get_clock().now().to_msg()
		self.vel_obj.vector.x = (self.NORMAL_LIN_VEL if(not act) else self.TRANS_LIN_VEL)
		self.vel_obj.vector.y = (act*ang_vel if act else 0)
		self.vel_obj.vector.z = 0.0
		return self.vel_obj
		
		
		
		


def main(args=None):
    # Instanciate our avoider object
    # Initialize our node
	rclpy.init(args=args)
	node = rclpy.create_node('avoider')
	avoider = Avoider(Vector3Stamped(),node)
	# Subscribe to the "/scan" topic in order to read laser scans data from it
	subscriber=node.create_subscription(LaserScan,"/scan", avoider.indentify_regions,10)
    #create our publisher that'll publish to the "/cmd_vel" topic
	pub = node.create_publisher(Vector3Stamped,"/cmd_vel",  10)

	rate = node.create_rate(10)
    
    #keep running while the ros-master isn't shutdown
	while rclpy.ok():
		pub.publish(avoider.avoid())
		rate.sleep()

if __name__ == "__main__":
    main()
